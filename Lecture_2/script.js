n1 = 9
n2 = 9.0
n3 = "9"

console.log(n1)
console.log(n2)
console.log(n3)
console.log(n1+n3)
console.log(n1+parseInt(n3))

console.log(Math)
x = 5.5
console.log(Math.round(x))
console.log(Math.ceil(x))
console.log(Math.floor(x))

console.log("=====")
r = Math.random()
console.log(r)
console.log(r*10)
console.log(Math.floor(r*10))