function task2_4(N, B){

    switch(B){
        case 1: document.write("Round - "+Math.round(N)+"<br>");
        break;
        case 2: document.write("Ceil - "+Math.ceil(N)+"<br>");
        break;
        case 3: document.write("Flour - "+Math.floor(N)+"<br>")
        break;
        default: document.write("The Boolean Parameter is invalid")
    }

    // if(B==1){
    //     document.write("Round - "+Math.round(N)+"<br>")
    // }else if(B==2){
    //     document.write("Ceil - "+Math.ceil(N)+"<br>")
    // }else if(B==3){
    //     document.write("Flour - "+Math.floor(N)+"<br>")
    // }else{
    //     document.write("The Boolean Parameter is invalid")
    // }
}

task2_4(5.2, 1) // 5
document.write("<hr>")
task2_4(5.2, 2) // 6
document.write("<hr>")
task2_4(5.2, 3) // 5
document.write("<hr>")
task2_4(5.2, 7) // 5