var buttons = document.getElementsByTagName("button");
// console.log(buttons);
var ball_colours = ["green", "red", "black", "brown"];
var square = document.getElementById("square");
const BALL_DURATION = 1000, MAX_POINT=50, MAX_TIME=5;
document.getElementById("timer").innerText = MAX_TIME;



buttons[0].addEventListener("click", function(){
    // console.log(3);
    this.setAttribute("disabled", "disabled");
    
    add_ball = setInterval(function(){
        var ball = document.createElement("div");
        ball.classList.add("ball");
        ball.style.backgroundColor = ball_colours[Math.floor(Math.random()*ball_colours.length)];
        ball.style.top = Math.random()*420+"px";
        ball.style.left = Math.random()*620+"px";
        ball.innerText = Math.floor(MAX_POINT*Math.random());
        square.appendChild(ball);
        ball.addEventListener("click", function(){
            var points = parseInt(buttons[1].innerText);
            points += parseInt(this.innerText);
            buttons[1].innerText = points; 
            this.parentElement.removeChild(this);
            // this.style.display = "none";
        })
    }, BALL_DURATION)

    timer_t = setInterval(function(){
        var timer = document.getElementById("timer");
        var t = parseInt(timer.innerText);
        t -= 1;
        timer.innerText = t;

        if(t==0){
            clearInterval(add_ball);
            clearInterval(timer_t);
            // square.innerHTML = "";
            for(let i=0; i<square.children.length; i++){
                var old_child = square.children[i];
                var new_child = old_child.cloneNode(true);
                console.log(new_child);
                square.replaceChild(new_child, old_child);
            }
        }

    }, 1000)

})
