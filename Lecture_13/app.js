var buttons = document.getElementsByTagName("button");
var square = document.getElementById("square");
var colours = ["brown", "black", "green", "blue"];
const ball_speed = 1000, timer_speed = 1000, refresh_time = 100000, max_point = 50;
document.getElementById("timer").innerText = 5;
// console.log(buttons);


buttons[0].addEventListener("click", function(){
    this.setAttribute("disabled", "disabled");
    // console.log("TEST");
    add_ball = setInterval(function(){
        var ball = document.createElement("div");
        // console.log(ball);
        ball.classList.add("ball");
        ball.style.backgroundColor = colours[Math.floor(Math.random()*colours.length)];
        ball.style.top = Math.random()*420+"px";
        ball.style.left = Math.random()*620+"px";
        ball.innerText = Math.floor(Math.random()*max_point);
        square.appendChild(ball);
        ball.addEventListener("click", function(){
            // console.log(this.parentElement);
            this.parentElement.removeChild(this);
            var points = parseInt(buttons[1].innerText);
            points += parseInt(this.innerText);
            buttons[1].innerText = points;
        });
    }, ball_speed);

    timer = setInterval(function(){
        var timer_span = document.getElementById("timer");
        var t = parseInt(timer_span.innerText);
        t -= 1;
        timer_span.innerText = t;
        if(t==0){
            clearInterval(timer);
            clearInterval(add_ball);
            for(let i=0; i<square.children.length; i++){
                var old_element = square.children[i];
                var new_element = old_element.cloneNode(true);
                old_element.parentNode.replaceChild(new_element, old_element);     
            }
            setTimeout(function(){
                location.reload();
            }, refresh_time)
        }
    }, timer_speed);
})


