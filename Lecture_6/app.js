
const generate = () => {
    var a = "abcde";
    var n = 20;
    var w = "";
    for(let i=0; i<n; i++){
        w += a[Math.floor(Math.random()*a.length)];
    }
    var r = document.getElementById("result");
    r.innerHTML = w;
}

function button_click_1(){
    alert("Mouse is clicked!!!");
}

function button_click_2(){
    // alert("Hello User!!");
    var u = document.getElementById("user-css");
    // var e = document.getElementById("email-css");
    // alert(u);
    console.log(u.value);
    // console.log(e);
    alert("Hello "+u.value+"!!!");
}

function button_click_3(){
    var u = document.getElementById("user-css");
    var r = document.getElementById("result");
    r.innerHTML = "Hello "+u.value+" !!!!";
}

const click_3 = function(){
    alert("Old Arrow Function");
}

const click_4 = () => {
    alert("New Arrow Function");
}

function f1(){
    n = 12;
    var n1 = 13;
    let n2 = 14;
    console.log("N= ", n, n1, n2);
    if(3<8){ 
        m = 120;
        var m1 = 130;
        let m2 = 140;
    }
    // console.log("M= ", m, m1, m2);
    console.log("M= ", m, m1);
}

function f2(){
    console.log("F2 - N= ", n);
}